<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <div class="row">
            <div class="col-lg-4">        
                <p><a class="app-logo"><?php echo \Yii::t('app', 'App name'); ?></a></p>
            </div>
            <div class="col-lg-8">
                <h1><?php echo \Yii::t('app', 'Zombie 56 days'); ?></h1>
            </div>
        </div>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>3</h2>
                <h3>Days to go</h3>
            </div>
            <div class="col-lg-4">
                <h2>1,000,000</h2>
                <h3>Steps to go</h3>
            </div>
            <div class="col-lg-4">
                <h2>4</h2>
                <h3>Team position</h3>
            </div>
        </div>

        <div class="row" id="social-outer">
            <div class="col-lg-12">
                <p class="social">Add activity for person</p>
            </div>
        </div>

        <div id="leaderboard" class="row">
            <div class="col-lg-12">
                <h4>Leaderboard</h4>
            </div>
        </div>    

        
        <?php 
        $id = 0;
        foreach ($teams as $team):
            // Explict increment
            $id = $id + 1;
            switch ($team->movement):
                case $team::UP:
                    $icon = '<a href="#"><span class="glyphicon glyphicon-arrow-up"></span></a>';
                    break;
                case $team::DOWN:
                    $icon = '<a href="#"><span class="glyphicon glyphicon-arrow-down"></span></a>';
                    break;
                case $team::NO_CHANGE:
                    $icon = '<a href="#"><span class="glyphicon glyphicon-arrow-left"></span></a>';
                    break;                    
            endswitch;
        ?>
        <div class="row">
            <div class="col-lg-1">&nbsp;</div>
            <div class="col-lg-2"><?php echo "{$id} {$icon}";?></div>
            <div class="col-lg-1">&nbsp;</div>
            <div class="col-lg-4"><?php echo "{$team->name} ($team->id)";?></div>
            <div class="col-lg-1">&nbsp;</div>
            <div class="col-lg-2"><?php echo $team->getScore(); ?> steps</div><?php //@todo Divide steps by by number of records for average ?>
            <div class="col-lg-1">&nbsp;</div>
        </div>
        <?php endforeach; ?>
 

        
    </div>
</div>
