<?php
/**
 * Manages bookings
 */
namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\commands;
use app\components;
use app\models;

/**
 * By default, yii\rest\ActiveController provides the following actions:
 *
 * @see http://www.yiiframework.com/doc-2.0/guide-rest-quick-start.html
 * 
 * index: list resources page by page;
 * view: return the details of a specified resource;
 * create: create a new resource;
 * update: update an existing resource;
 * delete: delete the specified resource;
 * options: return the supported HTTP methods.
 */
class PointController extends ActiveController
{
    public $modelClass = 'app\models\Point';

    public function actionTest()
    {
    	die('pass');
    }

}

?>