<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class Team extends ActiveRecord
{
    public $collection;
    const UP = 'up';
    const DOWN = 'down';
    const NO_CHANGE = 'no-change';
    /**
     * This would readd off a MySQL table really...
     * also restful
     * 
     */
    private static $teams = [
        '100' => [
            'id' => '100',
            'teamname' => 'Team 1',
            'movement' => self::DOWN,
        ],
        '101' => [
            'id' => '101',
            'teamname' => 'Foo team',
            'movement' => self::DOWN,
        ],
        '102' => [
            'id' => '102',
            'teamname' => 'The techies',
            // 'movement' => self::UP,
        ],
        '103' => [
            'id' => '103',
            'teamname' => 'Team Bar',
            'movement' => self::UP,
        ],
        '104' => [
            'id' => '104',
            'teamname' => 'Team Axe',
            'movement' => self::UP,
        ],
        '105' => [
            'id' => '105',
            'teamname' => 'The tigers',
            // 'movement' => self::UP,
        ],
        '106' => [
            'id' => '106',
            'teamname' => 'Hardcastle',
            'movement' => self::UP,
        ],        
    ];

    public function getPoints()
    {
        return $this->hasMany(\app\models\Point::className(), ['team_id' => 'id']);
    }

    public function getScore()
    {
        $points = $this->getPoints()->all();
        $total = 0;
        foreach ($points as $point)
        {
            $total = $total + intval($point->score);
        }
        return $total;
    }

    public function initFake()
    {
        $this->collection = [];
        foreach (self::$teams as $team)
        {
            $teamObject           = new self;
            $teamObject->id       = $team['id'];
            $teamObject->name     = $team['teamname'];
            $teamObject->movement = (isset($team['movement'])) ? $team['movement'] : self::NO_CHANGE;
            $this->collection[]   = $teamObject;
        }
        return $this;
    }

    public function getAll()
    {
        return $this->collection;
    }
}
