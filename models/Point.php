<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class Point extends ActiveRecord
{
    public $collection;
    const UP = 'up';
    const DOWN = 'down';
    const NO_CHANGE = 'no-change';

    /**
     * Definition of [MySQL] table name
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'point';
    }

    /**
     * Definition of table attributes
     * @return [type] [description]
     */
    public function rules()
    {
        return [
            [['team_id','score'], 'integer'],
        ];
    }


}
