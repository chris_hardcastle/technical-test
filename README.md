Technical test response
=======================

This project was build by a prospective contractor for FitBug on Wed 19th Jan (late at night). The main thing is: there's an end point to which points can be awarded already.

It was built from a framework but there's a good deal of coding, assets and PHP to demonstrate skills and style.

The project includes:

* RESTful API end points (as below) for awarding / removing points to teams
* ORM based model releationship between table data
* Fake collection of teams included as a static array - to save time as a demo
* Dynamic look up and calculation of team points by means of PHP model
* List of teams sorted by score (descending down)
* CSS demo / usage of bootstrap - mobile version of the design
* Around 2 hours and 20 minutes of development time... (I got carried away / had fun) (^_^)


Contact: Chris Hardcastle


Team point assignment
=====================
1) Add points
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://kiq.localhost/points" -d '{"team_id": 106, "score": 1234}'

3) Delete points
curl -X DELETE 'http://kiq.localhost/points/[ ID_OF_POINTS_RECORD ]'



CONFIGURATION
-------------

Please configure and build the encosed database in order to run the site locally and or add points.

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=[DATABASE_NAME]',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Run this query for point storage
================================


SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `point`
-- ----------------------------
DROP TABLE IF EXISTS `point`;
CREATE TABLE `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `point`
-- ----------------------------
BEGIN;
INSERT INTO `point` VALUES ('4', '106', '9999'), ('5', '106', '9'), ('7', '103', '77'), ('8', '103', '12'), ('9', '102', '24'), ('10', '104', '743'), ('11', '105', '125');
COMMIT;

-- ----------------------------
--  Table structure for `team`
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `movement` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
